# barcode4j
This fork of barcode4j is based on the avalon free fork from https://github.com/chunlinyao/barcode4j/

- No Avalon dependencies and support for newer XZing 3+
- Add ink spread support
